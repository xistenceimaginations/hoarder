# Hoarder

Opera (/Chrome) Plugin for organising/managing bookmarks with tags, quick search and more. This is a 'Lab'-project, done in Svelte and ment as Prove-Of-Concept, so nothing to by shiny and perfect nor to be maintained a lot.

## Hint

If you want to develop, i recommen to a) run this via 'npm run dev' and b) add this extension as 'unpacked extension' to your
browser. To successfully adding to you have to choose the 'public'-folder as folder when you have to select the folder for
the unpacked extension.

## Svelte

This was done in [Svelte](https://svelte.dev/), not maybe in the most perfect, shiny and optimal way, but it was a project to get myself into [Svelte](https://svelte.dev/). So if you want to get an example how to do things in this framework this might be
another choice. Everything else is just plain JS, CSS, HTML (so far).

## Risk

It is just a PoC, there might be bugs, you might loose data in some circumstances - i'm not perfect, i don't want to be.
So backup everything and judge/handle yourself what you want to do how and how much risk it may contain. I'm not responsible.

## Install

Just run `npm i`, after that give `npm run dev` a try, add the extension to your browser and play around. Or you just
dig in the code.

## Assets

So far there is an icon ship by svelte themself, a font-set you can get freely from https://fonts.google.com/specimen/Raleway

## Dependencies

Svelte. That's it.

## Some more information
Take a look here >
 [Svelte](https://svelte.dev/) | 
 [IndexedDB](https://developer.mozilla.org/de/docs/IndexedDB) |
 [Opera Addon Development](https://dev.opera.com/extensions/)