export default class Database {
    constructor(dbName, tableName, dbVersion, createCB, initCB) {
        this.tableName = tableName;
        this.hasIDB = typeof window.indexedDB != 'undefined';
        this.createCB = createCB;
        this.initCB = initCB;
        this.db = undefined;
        this.dbConnection = indexedDB.open(dbName, dbVersion);
        this.dbConnection.onsuccess = this.onSuccess.bind(this);
        this.dbConnection.onerror = this.onError.bind(this);
        this.dbConnection.onblocked = this.onBlocked.bind(this);
        this.dbConnection.onupgradeneeded = this.onUpgradeNeeded.bind(this);
        this.lastCacheTimeStamp = undefined;
        this.cache = [];
        this.tagCache = [];

        console.log('IndexedDB supported? ' + this.hasIDB);
    }

    get(key) {
        return new Promise((resolve, reject) => {
            // console.log('get 1');
            let transaction = this.db.transaction([this.tableName], 'readwrite');
            let objectStore = transaction.objectStore(this.tableName);
            let request = objectStore.get(key);

            request.onsuccess = e => {
                // console.log('get 2 ' + request.result);
                if (request.result) {
                    resolve(request.result);
                } else {
                    reject();
                }
                // console.log(request.result);
            };
            request.onerror = e => {
                // console.log('get 3');
                // console.log(e);
                reject();
            };
            // request.onblocked = e => {
            //     // console.log('get 4');
            //     // console.log(e);
            //     reject();
            // };
            // transaction.oncomplete = e => {
            //     console.log('get 5 ' + request.result);
            //     // console.log(request.result);
            //     resolve(request.result);
            // };
            // transaction.onerror = e => {
            //     reject();
            //     // console.log('get 6');
            // };
        });
    }

    add(key, value) {
        return new Promise((resolve, reject) => {
            // console.log('add 1');
            this.get(key).then(reject, entry => {
                // console.log('add 2');
                if (!entry) {
                    // console.log('add 3');
                    let transaction = this.db.transaction([this.tableName], 'readwrite');

                    let request = transaction.objectStore(this.tableName).add(value);
                    request.onsuccess = e => {
                        // console.log('add 4');
                        resolve();
                    };
                    request.onerror = e => {
                        // console.log('add 6');
                        // console.log(e);
                        reject();
                    };
                    transaction.onerror = reject;
                } else {
                    // console.log('add 5');
                    // console.log('entry exists');
                    reject();
                }
            }).catch(reject);
        });
    }

    count() {
        return new Promise((resolve, reject) => {
            this.db
                .transaction([this.tableName], 'readonly')
                .objectStore(this.tableName)
                .count()
                .onsuccess = e => resolve(e.target.result);
        });
    }

    updateMeta() {
        return new Promise((resolve, reject) => {
            console.log('update meta...');
            let transaction = this.db.transaction([this.tableName], 'readwrite');
            let request = transaction.objectStore(this.tableName).put({
                url: 'meta',
                lastChange: new Date().getTime()
            }/*, 'meta'*/);

            request.onsuccess = e => {
                console.log('meta updated');
                resolve();
            };
            request.onerror = e => {
                console.log('meta update failed');
                reject();
            };
            transaction.onerror = reject;
        });
    }

    getMeta() {
        return new Promise((resolve, reject) => {
            let transaction = this.db.transaction([this.tableName], 'readwrite');
            let request = transaction.objectStore(this.tableName).get('meta');

            request.onsuccess = e => {
                // console.log(request.result);
                resolve(request.result);
            };
            request.onerror = e => {
                console.log('not set');
            };
            transaction.onerror = reject;
        });
    }

    update(key, value) {
        return new Promise((resolve, reject) => {
            let transaction = this.db.transaction([this.tableName], 'readwrite');

            transaction.objectStore(this.tableName).put(value);
            transaction.oncomplete = e => {
                // this.get(key).then(entry => {
                resolve();
                // });
            };
            transaction.onerror = reject;
        });
    }

    delete(key) {
        return new Promise((resolve, reject) => {
            if (key) {
                let transaction = this.db.transaction([this.tableName], 'readwrite');

                transaction.objectStore(this.tableName).delete(key);
                transaction.oncomplete = e => {
                    resolve();
                };
                transaction.onerror = reject;
            }
        });
    }

    allTags() {
        return this.tagCache;
    }

    all(/*offset, total, */filter) {
        return new Promise((resolve, reject) => {
            this.getMeta().then(meta => {
                // console.log('meta:');
                // console.log(meta);
                if (meta.lastChange !== this.lastCacheTimeStamp) {
                    this.lastCacheTimeStamp = meta.lastChange;


                    // let data = [];
                    let hasSkipped = false;
                    let tagSet = new Set();
                    this.cache = [];
                    this.tagCache = [];


                    // let range = IDBKeyRange.bound('amazon', 'amazon' + 'z');
                    // let index = this.db.
                    //     transaction([this.tableName], 'readwrite').
                    //     objectStore(this.tableName).
                    //     index('url');

                    // index.openCursor(range).onsuccess = e => {
                    //     let cursor = e.target.result;

                    //     if (!hasSkipped && offset > 0) {
                    //         hasSkipped = true;
                    //         cursor.advance(offset);
                    //         return;
                    //     }
                    //     if (cursor) {
                    //         data.push({ key: cursor.key, value: cursor.value });

                    //         if (data.length < total) {
                    //             cursor.continue();
                    //         } else {
                    //             resolve(data);
                    //         }
                    //     } else {
                    //         resolve(data);
                    //     }
                    // };
                    // let internalIndex = offset;
                    this.db.
                        transaction([this.tableName], 'readwrite').
                        objectStore(this.tableName).
                        openCursor(/*IDBKeyRange.bound('amazon', 'amazon' + 'z')*/).
                        onsuccess = e => {
                            let cursor = e.target.result;

                            // if (!hasSkipped && offset > 0) {
                            //     hasSkipped = true;
                            //     cursor.advance(offset);
                            //     return;
                            // }
                            if (cursor) {
                                // if (filter(cursor)) {
                                this.cache.push({ key: cursor.key, value: cursor.value });


                                if (cursor.value.tags) {
                                    let tags = cursor.value.tags.
                                        split(' ').
                                        map(tag => tag.trim().toLowerCase()).
                                        filter(tag => tag.length > 0).
                                        forEach(tag => {
                                            tagSet.add(tag);
                                            // if(this.tagCache.indexOf(tag)){
                                            //     this.tagCache.push(tag);
                                            // }
                                        });
                                }

                                // data.push({ key: cursor.key, value: cursor.value });
                                // }

                                // internalIndex++;

                                // if (data.length < total) {
                                cursor.continue();
                                // } else {
                                //     resolve({ entry: data, lastIndex: internalIndex });
                                // }
                            } else {
                                this.tagCache = Array.from(tagSet);


                                resolve(
                                    this.cache
                                    // { entry: data, lastIndex: -1/*internalIndex*/ }
                                );
                            }
                        };
                    // console.log('update cache');
                } else {
                    console.log('use cache');
                    resolve(this.cache);
                }
            });


            // let data = [];
            // let hasSkipped = false;


            // // let range = IDBKeyRange.bound('amazon', 'amazon' + 'z');
            // // let index = this.db.
            // //     transaction([this.tableName], 'readwrite').
            // //     objectStore(this.tableName).
            // //     index('url');

            // // index.openCursor(range).onsuccess = e => {
            // //     let cursor = e.target.result;

            // //     if (!hasSkipped && offset > 0) {
            // //         hasSkipped = true;
            // //         cursor.advance(offset);
            // //         return;
            // //     }
            // //     if (cursor) {
            // //         data.push({ key: cursor.key, value: cursor.value });

            // //         if (data.length < total) {
            // //             cursor.continue();
            // //         } else {
            // //             resolve(data);
            // //         }
            // //     } else {
            // //         resolve(data);
            // //     }
            // // };
            // // let internalIndex = offset;

            // this.db.
            //     transaction([this.tableName], 'readwrite').
            //     objectStore(this.tableName).
            //     openCursor(/*IDBKeyRange.bound('amazon', 'amazon' + 'z')*/).
            //     onsuccess = e => {
            //         let cursor = e.target.result;

            //         // if (!hasSkipped && offset > 0) {
            //         //     hasSkipped = true;
            //         //     cursor.advance(offset);
            //         //     return;
            //         // }
            //         if (cursor) {
            //             // if (filter(cursor)) {
            //             data.push({ key: cursor.key, value: cursor.value });
            //             // }

            //             // internalIndex++;

            //             // if (data.length < total) {
            //             cursor.continue();
            //             // } else {
            //             //     resolve({ entry: data, lastIndex: internalIndex });
            //             // }
            //         } else {
            //             resolve({ entry: data, lastIndex: -1/*internalIndex*/ });
            //         }
            //     };
        });
    }

    onSuccess(e) {
        console.log('onSuccess');
        this.db = e.target.result;

        if (this.initCB) {
            this.initCB(this.db);
        }
    }

    onError(e) {
        console.log('onError');
    }

    onBlocked(e) {
        console.log('onBlocked');
    }

    onUpgradeNeeded(e) {
        console.log('onUpgradeNeeded');
        console.log(e);
        if (e.oldVersion < 1) {
            this.db = e.target.result;

            if (this.createCB) {
                this.createCB(this.db);
            }
        }
    }

    clear() {
        return new Promise((resolve, reject) => {
            let transaction = this.db.transaction([this.tableName], 'readwrite');

            transaction.objectStore(this.tableName).clear();
            transaction.oncomplete = e => {
                this.updateMeta().then(() => {
                    resolve();
                });
            };
            transaction.onerror = reject;
        });
    }

    exportContent() {
        return new Promise((resolve, reject) => {
            this.all().then(collection => {
                let content = '';

                collection.forEach(entry => {
                    content += entry.key + '\t' +
                        (entry.value.title || '') + '\t' +
                        (entry.value.tags || '') + '\t' +
                        (entry.value.check || '') + '\t' +
                        (entry.value.icon || '');
                    content += '\n';
                });

                resolve(content);
            });
        });
    }
}
