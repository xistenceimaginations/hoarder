export default class Filter {

    constructor(str) {
        if (str.length > 1 && str.charAt(0) === '#') {
            this.type = 'tags';
            this.term = str.substring(1).trim().toLowerCase();
        } else if (str.length > 1 && str.charAt(0) === '@') {
            this.type = 'url';
            this.term = str.substring(1).trim().toLowerCase();
        } else {
            this.type = 'title';
            this.term = str.trim().toLowerCase();
        }
    }

    matches(entry) {
        if (this.term) {
            switch (this.type) {
                case 'url':
                    return entry.url && entry.url.trim().toLowerCase().indexOf(this.term) !== -1;
                case 'tags':
                    return entry.tags && entry.tags.trim().toLowerCase().indexOf(this.term) !== -1;
                case 'title':
                    return entry.title && entry.title.trim().toLowerCase().indexOf(this.term) !== -1;
                default:
                    return false;
            }
        }
        return false;
    }
}