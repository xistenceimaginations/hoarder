import App from './App.svelte';
import EventBus from './EventBus';

// setting up a global event bus for component communication
window.eventBus = new EventBus();
window.$on = window.eventBus.addEventListener.bind(window.eventBus);
window.$emit = window.eventBus.dispatchEvent.bind(window.eventBus);
window.$i18n = key => {
	return 'foo';
};

var app = new App({
	target: document.body
});

export default app;